//*****************************************************************************
// main.c
// Author: jkrachey@wisc.edu
//*****************************************************************************
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#include "TM4C123.h"
#include "headout.h"

#define NUM_NODES     20

extern void serialDebugInit(void);
char StudentName[] ="Coda Phillips";

// Linked node data structure
struct node {
	char id; // Single byte for ID
	char *payload; // Pointer into INTERCEPT packet payload
	char size; // Size of payload
	struct node *next; // Pointer to next node
};

// Index into INTERCEPT, helps emulate reading a file or socket, no random access
int i = 0;

// Head node, no actual data, but simplifies data structure operations
struct node HEAD;

// Pointer to tail node of linked list, no need to iterate through list every time
struct node *tail = &HEAD;

// Handle packet of type 0xDEAD as defined in spec
void
dead(struct node *tail){	
	tail->next = NULL;
	tail->size = INTERCEPT[i];
	i++;
	tail->id = INTERCEPT[i];
	i++;
	tail->payload = &INTERCEPT[i];
	i += tail->size;
}

// Handle packet of type 0xBEEF as defined in spec
void
beef(struct node *tail){
	i += 3; // Garbage
	tail->next = NULL;
	tail->id = INTERCEPT[i];
	i++;
	tail->size = INTERCEPT[i];
	i++;
	tail->payload = &INTERCEPT[i];
	i += tail->size;
}

// Handle packet of type 0x1DEA as defined in spec
void
idea(struct node *tail){
	tail->next = NULL;
	tail->id = INTERCEPT[i];
	i++;
	tail->size = INTERCEPT[i];
	i++;
	i += 2; //Garbage
	tail->payload = &INTERCEPT[i];
	i += tail->size;
}

//*****************************************************************************
//*****************************************************************************
int 
main(void)
{
	// Pointer to current node while traversing
	struct node *curr;
	int j;
	int k;
	
  serialDebugInit();
  printf("\n\r");
  printf("**************************\n\r");
  printf(" ECE353 - HW2    \n\r");
  printf(" %s\n\r",StudentName);
  printf("**************************\n\r");
	
	// Get packets
	while(INTERCEPT[i] != 0x00){
		// if found 0xDEAD
		if(INTERCEPT[i] == '\xAD' && INTERCEPT[i+1] == '\xDE'){
			// Allocate a new node at the end of the list
			tail->next = malloc(sizeof(struct node));
			if(tail->next == NULL){
				printf("Error\n");
				return -1;
			}
			// Iterate tail by one node
			tail = tail->next;
			// Account for reading two bytes for packet type
			i += 2;
			// populate node with packet info
			dead(tail);
		}
		// if found 0xBEEF
		else if (INTERCEPT[i] == '\xEF' && INTERCEPT[i+1] == '\xBE'){
			// Allocate a new node at the end of the list
			tail->next = malloc(sizeof(struct node));
			if(tail->next == NULL){
				printf("Error\n");
				return -1;
			}
			// Iterate tail by one node
			tail = tail->next;
			// Account for reading two bytes for packet type
			i+=2;
			// populate node with packet info
			beef(tail);
		}
		// if found 0x1DEA
		else if (INTERCEPT[i] == '\xEA' && INTERCEPT[i+1] == '\x1D'){
			// Allocate a new node at the end of the list
			tail->next = malloc(sizeof(struct node));
			if(tail->next == NULL){
				printf("Error\n");
				return -1;
			}
			// Iterate tail by one node
			tail = tail->next;
			// Account for reading two bytes for packet type
			i+=2;
			// populate node with packet info
			idea(tail);
		}
		// Garbage
		else{
			// Iterate to next byte
			i++;
		}
	}
	
	// Parse unsorted list for packet with sequential id
	// Basically selection sort but without making the sorted list. O(n^2) is good enough
	// when n <= 256
	for(j=0;j<256;j++){
		// For each ID in order
		curr = &HEAD;
		// Search the list for that ID
		while(curr->next != NULL){
			if(curr->next->id == j){
				// When found print the payload, one char at a time
				for(k=0;k<curr->next->size;k++){
					printf("%c",curr->next->payload[k]);
				}
				free(curr->next);  // Remove the used node from the list
				curr->next = curr->next->next;
				break;
			}
			curr = curr->next;
		}
	}
  
  while(1){};
}
